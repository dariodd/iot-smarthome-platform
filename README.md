# IoT SmartHome Platform

## Advanced Programming Language project

Il progetto tratta la gestione di dispositivi IoT posizionati all'interno della home automation.

L'implementazione è caratterizzata dall'utilizzo di 3 linguaggi:
	- Scala
	- Python
	- C++

Nella main directory è presente il pdf "[relazione](Documentazione.pdf)" con la descrizione del progetto.

Di seguito sono riportati i comandi per eseguire il progetto, le componenti principali sono già state deployate su AWS Cloud, si ha quindi la possibilità di eseguire il progetto senza necessità di deployare il backend dell'applicazione.

#### Tutti i comandi sono stati testati su un ambiente di sviluppo Linux-based (Ubuntu 18.04)

## Python - Client
#### Desktop-version
E' consigliato l'utilizzo di un virtual environment per l'esecuzione del client in ambiente desktop.

Requisiti:
- python >= 3.6
- python3-pip
- xclip (opzionale)

Librerie utilizzate:  
- Kivy
- pandas
- matplotlib
- requests

```sh
    cd Python
    python3 -m pip install --upgrade --user pip setuptools virtualenv
    python3 -m pip install -r requirements.txt
    python3 main.py
```

#### Mobile-version
La versione mobile è stata compilata utilizzando il tool [Buildozer](https://www.github.com/kivy/buildozer), è possibile trovare già il file apk da installare per android [qui](Python/android-version/build)

## Scala - Server
Una versione funzionante del server è online su cloud Amazon AWS, all'IP 3.209.53.237, DNS ec2-3-209-53-237.compute-1.amazonaws.com, porta 8080, il client e il device sono già configurati per utilizzarla, non è quindi necessario installarlo in locale.  

Per installarla localmente è possibile seguire la seguente procedura:  
Requisiti:
- maven 3
- jvm 1.8
- scala 2.11.12

Librerie utilizzate:  
- spring-boot framework
- itextpdf
- json4s
- jfreechart
- spring-kafka

```sh
cd Scala
mvn package -P production  -Dmaven.test.skip=true
cd target
scala ScalaProjectAPL-1.0-SNAPSHOT.jar
```

#### Docker-version
Se si vuole eseguire tramite docker è presente all'interno della directory Scala il dockerfile necessario per l'esecuzione

```sh
cd Scala
docker build -t aplproject:v1 .
docker run aplproject:v1 -p 8080:8080
```

## C++ - Client
Ci sono 2 versioni del codice, una appositamente progettata per essere eseguita su raspberry con sensori reali e una generica che simula i dati dei sensori.

#### Versione simulata
Requisiti:
- libssl
- cppkafka (https://github.com/mfontanini/cppkafka)
- rdkafka
- libboost
- g++

Librerie utilizzate:  
-cppkafka  

```sh
cd C++/C++
ldconfig
make
./rspclient <OPTIONS>
```

Le opzioni sono:
1. -i "Device ID"
2. -c "Tempo di Campionamento"

#### RaspberryPi-version
Requisiti:
- cppkafka
- rdkafka
- wiringPi
- g++

Dispositivi:
- 1x Dht 11
- 1x photoresistor
- 1x button
- 4x led

Questa versione può essere eseguita solo su RaspberryPi e necessita la corretta configurazione dei pin all'interno del file [constants.h](C++/C++(raspberry)/constants.h).
