import requests
from requests.auth import HTTPBasicAuth


class JsonSerializable(object):
    def toJson(self):
        return self.__dict__


class Auth:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def getbasicauth(self):
        return HTTPBasicAuth(self.username, self.password)

    def getusername(self):
        return self.username
    
    def getpassword(self):
        return self.password


class RestClient:
    def __init__(self, url, auth=None):
        self.url = url
        self.auth = auth

    def postrequest(self, path, json):
        r = requests.post(self.url+path, json=json, auth=self.auth)
        if 200 <= r.status_code < 300:
            return True
        else:
            return False

    def getrequest(self, path):
        r = requests.get(self.url+path, auth=self.auth)
        if 200 <= r.status_code < 300:
            return r.content
        else:
            return None

    def setauth(self, auth):
        self.auth = auth

    def deleteauth(self):
        self.auth = None


class User(JsonSerializable):
    def __init__(self, username, password, email=None):
        self.username = username
        self.password = password
        self.email = email


class Device(JsonSerializable):
    def __init__(self, id):
        self.idDevice = id
