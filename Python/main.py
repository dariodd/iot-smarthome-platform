import re
import threading
import json

from kivy.app import App
from kivy.clock import Clock
from kivy.properties import StringProperty
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from functools import partial
from model import User, RestClient, Auth

import pandas as pd
from popups import LoadingPop, RegistrationPop, RegistrationDevicePop


url = "http://ec2-3-209-53-237.compute-1.amazonaws.com:8080"
#url = "http://localhost:8080"


class DevicePage(Screen):
    device_id, status, user, mac = StringProperty(), StringProperty(), StringProperty(), StringProperty()
    sensor1, sensor2, sensor3 = StringProperty(), StringProperty(), StringProperty()
    timestamp = StringProperty()
    clock = None
    samplingTime = 30

    def load_stats(self, iddevice):
        p = LoadingPop()
        p.open()
        load_thread = threading.Thread(target=self.load_deviceinfo, args=(p, iddevice))
        load_thread.start()

    def load_deviceinfo(self, pop, iddevice, *largs):
        request = restclient.getrequest("/device/iddevice/"+iddevice)
        if request is not None:
            device = json.loads(request)
            self.device_id = device['idDevice']
            self.status = device['status']
            self.user = device['user']
            self.mac = device['mac']
            try:
                tmp = device['time']
                if tmp >= 30:
                    self.samplingTime = (tmp * 2) +1
            except:
                pass
        else:
            p = RegistrationPop()
            p.ids.reg_layout.remove_widget(p.ids.login_but)
            p.set_string("Device Retrieve Failed")
            p.open()
        request = restclient.getrequest("/data/id/" + iddevice)
        if request is not None:
            data = json.loads(request)
            self.sensor1 = str(round(data['temperature'], 2))
            if data['stateAirConditioner'] == "on":
                self.ids["status1"].state = "down"
            else:
                self.ids["status1"].state = "normal"
                self.ids["status1"].text = "off"
            self.sensor2 = str(round(data['humidity'], 2))
            if data['stateHumidifier'] == "on":
                self.ids["status2"].state = "down"
            else:
                self.ids["status2"].state = "normal"
                self.ids["status2"].text = "off"
            self.sensor3 = str(round(data['brightness'], 2))
            if data['stateLight'] == "on":
                self.ids["status3"].state = "down"
            else:
                self.ids["status3"].state = "normal"
                self.ids["status3"].text = "off"
            self.timestamp = data['timestamp_local']
        else:
            p = RegistrationPop()
            p.ids.reg_layout.remove_widget(p.ids.login_but)
            p.set_string("Data Retrieve Failed")
            p.open()
        if pop is not None:
            if self.clock is None:
                self.clock = Clock.schedule_interval(partial(self.load_deviceinfo, None, iddevice), self.samplingTime)
            pop.dismiss()

    def send_message(self, button):
        if button.name == "temperature":
            if button.state == "down":
                json_string = {"conditioner": "on"}
            else:
                json_string = {"conditioner": "off"}
        elif button.name == "brightness":
            if button.state == "down":
                json_string = {"light": "on"}
            else:
                json_string = {"light": "off"}
        elif button.name == "humidity":
            if button.state == "down":
                json_string = {"humidifier": "on"}
            else:
                json_string = {"humidifier": "off"}
        else:
            return
        p = LoadingPop()
        p.open()

        def send_command(device_id):
            r = restclient.postrequest("/data/id/"+device_id+"/commands", json=json_string)
            if r:
                self.clock.cancel()
                self.clock = Clock.schedule_interval(partial(self.load_deviceinfo, None, self.device_id), self.samplingTime)
            p.dismiss()

        load_thread = threading.Thread(target=send_command, args=(self.device_id,))
        load_thread.start()

    def download_stats(self):
        try:
            interval = int(self.ids.interval_text.text)
        except:
            return
        if 90 >= interval > 0:
            r = restclient.getrequest("/data/id/" + self.device_id + "/time/" + str(interval))
            if not r:
                p = RegistrationPop()
                p.ids.reg_layout.remove_widget(p.ids.login_but)
                p.set_string("No Data Found")
                p.open()
                return
            df = pd.DataFrame(json.loads(r))
            df['timestampserver'] = pd.to_datetime(df['timestampserver'])
            df['timestamp_local'] = pd.to_datetime(df['timestamp_local'])
            df = df.sort_values(by=['timestamp_local'], ascending=True)
            ax = df.plot.line(x='timestamp_local', y=['temperature', 'humidity', 'brightness'])
            ax.set_xlabel("timestamp")
            ax.legend(["Temperature C", "Humidity %RH", "Brightness %"])
            ax.set_ylim(0, 100)
            ax.figure.savefig("graph.png")
            App.get_running_app().root.get_screen('graph_page').add("graph.png")
            self.manager.current = "graph_page"


    def back(self):
        if self.clock is not None:
            self.clock.cancel()
        self.manager.current = "user_page"


class GraphPage(Screen):
    def add(self, graph):
        self.ids.grp.source = graph


class LoginPage(Screen):

    def verify_credentials(self):
        global auth
        username = self.ids["login"].text
        password = self.ids["password"].text
        auth = Auth(username, password)
        restclient.setauth(auth.getbasicauth())
        p = LoadingPop()
        p.open()
        load_thread = threading.Thread(target=self.login_test, args=(p, username))
        load_thread.start()

    def login_test(self, pop, username):
        r = restclient.getrequest("/user/username/"+username)
        if r is not None:
            self.manager.get_screen('user_page').load_device()
            self.manager.current = "user_page"
        else:
            restclient.deleteauth()
            p = RegistrationPop()
            p.ids.reg_layout.remove_widget(p.ids.login_but)
            p.set_string("Login Failed")
            p.open()
        pop.dismiss()

    def register(self):
        self.manager.current = "register_page"


class UserPage(Screen):
    num_buttons = 0

    def load_device(self):
        self.num_buttons = 0
        p = LoadingPop()
        p.open()
        load_thread = threading.Thread(target=self.device_retrieve, args=(p,))
        load_thread.start()

    def device_retrieve(self, pop):
        request = restclient.getrequest("/device/username/" + auth.getusername())
        if request is not None:
            device = json.loads(request)
            for dev in device:
                button = Button(text="Device "+dev['idDevice'], on_release=partial(self.switch_screen, dev['idDevice']))
                self.ids["run_buttons"].add_widget(button)
                self.num_buttons += 1
        pop.dismiss()

    def switch_screen(self, *args):
        self.manager.get_screen('device_page').load_stats(args[0])
        self.manager.current = "device_page"

    def register_device(self):
        p = RegistrationDevicePop(rest=restclient)
        p.open()

    def refresh_device(self):
        self.delete_log()
        self.load_device()

    def delete_log(self):
        for i in range(self.num_buttons, 0, -1):
            try:
                self.ids.run_buttons.remove_widget(self.ids.run_buttons.children[i - 1])
            except:
                pass

    def logout(self):
        self.delete_log()
        restclient.deleteauth()
        self.manager.current = "login_page"


class RegisterPage(Screen):
    def register_user(self):
        username = self.ids["r_username"]
        password = self.ids["r_pass"]
        email = self.ids["r_email"]
        if username.text == "":
            username.background_color = (1, 0, 0, .5)
            username.hint_text = "Empty Field"
            return
        elif password.text == "" and len(password.text) < 8:
            password.background_color = (1, 0, 0, .5)
            password.hint_text = "At least 8 char"
            password.text = ""
            return
        elif not re.match(r"[^@]+@[^@]+\.[^@]+", email.text):
            email.background_color = (1, 0, 0, .5)
            email.hint_text = "Valid Email"
            email.text = ""
            return
        user = User(username.text, password.text, email.text)
        p = LoadingPop()
        p.open()
        sender_thread = threading.Thread(target=self.send_user, args=(user, p))
        sender_thread.start()

    def send_user(self, user, pop):
        reg = restclient.postrequest("/user/register", user.toJson())
        pop.dismiss()
        p = RegistrationPop()
        if reg:
            p.set_string("Registration successful")
        else:
            p.set_string("Registration failed")
        p.open()


class ScreenManagement(ScreenManager):
    def __init__(self, **kwargs):
        super(ScreenManagement, self).__init__(**kwargs)
        self.current = 'login_page'


class LoginApp(App):
    def build(self):
        return ScreenManagement()


if __name__ == '__main__':
    restclient = RestClient(url)
    auth = None
    LoginApp().run()
