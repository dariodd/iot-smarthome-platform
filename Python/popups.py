import threading

from kivy.uix.popup import Popup
from kivy.properties import StringProperty
from model import Device


class LoadingPop(Popup):
    pass


class RegistrationPop(Popup):
    registration_text = StringProperty()

    def set_string(self, registration_text):
        self.registration_text = registration_text


class RegistrationDevicePop(Popup):

    def __init__(self, rest, *args, **kwargs):
        super(RegistrationDevicePop, self).__init__(*args, **kwargs)
        self.restclient = rest

    def add_device(self):
        device_id = self.ids["device_id"].text
        device = Device(device_id)
        p = LoadingPop()
        p.open()
        sender_thread = threading.Thread(target=self.send_device, args=(device, p))
        sender_thread.start()
        self.dismiss()

    def send_device(self, device, pop):
        reg = self.restclient.postrequest("/device/register", device.toJson())
        pop.dismiss()
        p = RegistrationPop()
        p.ids.reg_layout.remove_widget(p.ids.login_but)
        if reg:
            p.set_string("Registration successful\n\nPress Button on Device")
        else:
            p.set_string("Registration failed")
        p.open()