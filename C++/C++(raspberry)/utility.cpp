#include "utility.hpp"
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

namespace util {
    std::string get_timestamp() {
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];

        time(&rawtime);
        timeinfo = localtime(&rawtime);

        strftime(buffer, sizeof(buffer), "%d-%m-%Y_%H:%M:%S", timeinfo);
        std::string str(buffer);

        return str;

    }

    std::string retrieveMac(){
        std::string data;
        FILE * stream;
        const int max_buffer = 256;
        char buffer[max_buffer];

        stream = popen("cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address", "r");
        if (stream) {
            while (!feof(stream))
                if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
            pclose(stream);
        }
        std::replace(data.begin(), data.end(), ':', '-');
        return data.substr(0,17);
    }
}
