#ifndef C_SAMPLER_H
#define C_SAMPLER_H

#include <thread>
#include "utility.hpp"

using namespace std;

namespace smplr{

    class Sampler {
    private:
        static Sampler *instance;
        Sampler();
        float temperature;
        float humidity;
        float luminosity;
        int data_pin;
        int lux_pin;
        int time;
        void read_dht_data();
    public:
        static Sampler* getInstance();
        void run(int sampling_time);
        float get_temperature();
        float get_humidity();
        float get_luminosity();
    };



    void set_actuator(bool val, util::action pin);

}



#endif
