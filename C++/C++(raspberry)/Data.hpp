#ifndef C_DATA_H
#define C_DATA_H

#include <string>
#include <forward_list>
#include "sampler.hpp"

using namespace std;
using namespace smplr;

namespace data {

    class Data {

    public:
        Data(string measure_name, string state_actuator_name, string actuator_name, bool stateActuator);
		
        float get_lastMeasure();
        
        string get_lastActuatorState();

        string get_measureName();

        string get_stateActuatorName();

        string get_actuatorName();

        virtual float get_measure() = 0;

        virtual string get_stateActuator() = 0;

        virtual void set_stateActuator(string command) = 0;

    protected:
        Sampler *sampler;
        float last_measure;
        bool last_stateActuator;
        string measure_name;
        string state_actuator_name;
        string actuator_name;
        bool stateActuator;

    };

    class Data_Temp : public Data {

		public:
			Data_Temp();

			float get_measure() override;

			string get_stateActuator() override;

			void set_stateActuator(string command) override;
    };

    class Data_Hum : public Data {

    public:
        Data_Hum();

        float get_measure() override;

        string get_stateActuator() override;

        void set_stateActuator(string command) override;
    };

    class Data_Bright : public Data {

    public:
        Data_Bright();

        float get_measure() override;

        string get_stateActuator() override;

        void set_stateActuator(string command) override;
    };


    string toJson(string idDevice, string timestamp_local, forward_list<Data *> &sensors);
}

#endif
