#include "kafka.hpp"
#include <string>
#include <iostream>
#include <cppkafka/cppkafka.h>
#include <thread>

using namespace cppkafka;
using namespace std;


KafkaProducer::KafkaProducer(Configuration conf) : producer(conf) {
    producer.set_produce_success_callback([](const Message &msg) {
        cout << "Successfully produced message with payload " << msg.get_payload() << endl;
    });

    producer.set_produce_failure_callback([](const Message &msg) {
        cout << "Failed to produce message with payload " << msg.get_payload() << endl;
        return false;
    });
};

int KafkaProducer::send(string message, string topic_name) {
    MessageBuilder builder(topic_name);

    cout << "Producing messages into topic " << topic_name << endl;

    builder.payload(message);
    producer.add_message(builder);
    producer.flush();
    return true;
};


KafkaConsumer::KafkaConsumer(Configuration conf) : consumer(conf) {
    consumer.set_assignment_callback([](const TopicPartitionList &partitions) {
        cout << "Got assigned: " << partitions << endl;
    });

    consumer.set_revocation_callback([](const TopicPartitionList &partitions) {
        cout << "Got revoked: " << partitions << endl;
    });
};

bool KafkaConsumer::subscribe_topic(string topic_name) {
    consumer.subscribe({topic_name});

    cout << "Consuming messages from topic " << topic_name << endl;
    return true;
};

string KafkaConsumer::poll() {
    string res = "nope";
    Message msg = consumer.poll();

    if (msg) {
        if (msg.get_error()) {
            if (!msg.is_eof()) {
                res="error";
            }
        } else {
            if (msg.get_key()) {
                cout << msg.get_key() << " -> ";
            }
            res = msg.get_payload();
            consumer.commit(msg);
        }
    }
    return res;
};

bool KafkaConsumer::unsubscribe() {
    consumer.unsubscribe();
    return true;
};
