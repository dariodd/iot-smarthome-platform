#include "LinkedList.hpp"
#include <fstream>
#include <iostream>



namespace linklist {

    LinkedList::LinkedList() {
        head = NULL;
        tail = NULL;
        iterator = NULL;
    }

    LinkedList::~LinkedList(){
        node *tmp = head;

        while(tmp) {
            node *deleteMe = tmp;
            tmp = tmp->next;
            delete deleteMe;
        }
    }

    void LinkedList::add_node(std::string n) {
        node *tmp = new node;
        tmp->data = n;
        tmp->next = NULL;

        if (head == NULL) {
            head = tmp;
            tail = tmp;
        } else {
            tail->next = tmp;
            tail = tail->next;
        }
    }

    void LinkedList::save(std::string name_file) {
        std::ofstream ofs(name_file);
        if (head == NULL)
            return;
        else {
            node *tmp = head;
            while (tmp) {
                ofs << tmp->data << std::endl;
                tmp = tmp->next;
            }
        }
    }

    void LinkedList::load(std::string name_file) {
        std::ifstream iFile(name_file);
        if (!iFile.is_open()) {
            std::cerr << "file " << name_file << "not exists" << std::endl;
            return;
        }
        std::string line;
        while (getline(iFile, line)) {
            add_node(line);
        }

    }

    void LinkedList::print() {
        node *tmp = head;
        while (tmp != tail) {
            std::cout << tmp->data << std::endl;
            tmp = tmp->next;
        }
        std::cout << tmp->data << std::endl;

    }

    bool LinkedList::find(std::string id) {
        node *tmp = head;
        while (tmp != tail) {

            if (tmp->data.compare(id) == 0)
                return true;
            tmp = tmp->next;
        }
        if (tmp->data.compare(id) == 0)
            return true;
        return false;
    }

}
