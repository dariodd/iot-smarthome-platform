#ifndef C_UTILITY_H
#define C_UTILITY_H

#include <string>

namespace util {
    std::string get_timestamp();

    enum action {TEMP = 1, HUM = 2, LIGHT = 3};

    std::string retrieveMac();
}

#endif
