#define KAFKA_BROKER_IP "3.209.53.237"
#define KAFKA_BROKER_DNS "ec2-3-209-53-237.compute-1.amazonaws.com"
#define KAFKA_BROKER_PORT "9092"
#define KAFKA_TOPIC_REGISTRATION "register"
#define KAFKA_TOPIC_SEND "record_submission"
#define DEFAULT_PRODUCTION_CYCLE 30

#define REGISTRATION_LED 24
#define BUTTON 25
#define LIGHT_LED 23 //yellow
#define CONDITIONER_LED 22 //blue
#define HUMIDIFIER_LED 21 //green
#define DHT_PIN 7
#define LUX_PIN 1
