#include "sampler.hpp"
#include <wiringPi.h>
#include <iostream>
#include <cstdlib>
#include <cstdint>
#include <thread>
#include "constants.h"


using namespace std;
using namespace util;

#define MAX_TIMINGS 85



namespace smplr{

    const int dht_pin = DHT_PIN;
    const int light_pin = LUX_PIN;
    const int light_led = LIGHT_LED;
    const int conditioner_led = CONDITIONER_LED;
    const int humidifier_led = HUMIDIFIER_LED;
    const int sampling_time = DEFAULT_PRODUCTION_CYCLE

    Sampler* Sampler::instance = NULL;

    Sampler::Sampler(){
        data_pin = dht_pin;
        lux_pin = light_pin;
        this->run(sampling_time);
    }


    Sampler * Sampler::getInstance() {
        if (instance == NULL){
            instance = new Sampler();
        }
        return instance;
    }

    void Sampler::read_dht_data(){
        int data[5] = {0, 0, 0 ,0 ,0};
        int count = 0;
        double percentage;
        while(1){
            uint8_t laststate = HIGH;
            uint8_t counter = 0;
            uint8_t j = 0, i;

            data[0] = data[1] = data[2] = data[3] = data[4] = 0;

            pinMode(data_pin, OUTPUT);
            digitalWrite(data_pin, LOW);
            delay( 18 );

            pinMode(data_pin, INPUT);

            for (i =0; i< MAX_TIMINGS; i++){
                counter=0;
                while(digitalRead(data_pin) == laststate){
                    counter++;
                    delayMicroseconds(1);
                    if(counter==255){
                        break;
                    }
                }
                laststate = digitalRead (data_pin);
                if (counter == 255)
                    break;
                if( (i>=4) && (i%2 == 0)){
                    data[j/8] <<= 1;
                    if(counter > 32)
                        data[j/8] |= 1;
                    j++;
                }
            }


            if((j>=40) &&(data[4] == ((data[0]+data[1]+data[2]+ data[3]) & 0xFF))){
                float h = (float)((data[0] << 8) + data[1])/10;
                if(h>100){
                    h = data[0];
                }
                float c = (float)(((data[2] & 0x7F) << 8) + data[3]);
                if (c > 125){
                    c=data[2];
                }
                if(data[2] & 0x80){
                    c= -c;
                }
                cout<<"Humidity = "<<h<<" % Temperature = "<<c<<" *C"<<endl;
                temperature = c;
                humidity = h;
            }

            pinMode(lux_pin, OUTPUT);
            digitalWrite(lux_pin,0);
            delay(10);
            pinMode(lux_pin, INPUT);
            while(digitalRead(lux_pin) == 0)
                count=count+1;

            percentage = 1 - ((count - 500) / (double) (950000-500));
            if(percentage < 0 ) percentage = 0;
            luminosity = percentage * 100;

            cout<<"Luminosity = " << percentage << " %"<<endl;
            count = 0;
            this_thread::sleep_for(std::chrono::seconds(time/2));
        }
    }

    void Sampler::run(int sampling_time){
        time = sampling_time;
        thread t1(&Sampler::read_dht_data, this);
        t1.detach();
    }

    float Sampler::get_humidity(){
        return humidity;
    }

    float Sampler::get_luminosity(){
        return luminosity;
    }

    float Sampler::get_temperature(){
        return temperature;
    }

    void set_actuator(bool val, util::action pin){
		int act;
        switch(pin){
            case LIGHT:
                act=light_led;
                break;
            case HUM:
                act=humidifier_led;
                break;
            case TEMP:
                act=conditioner_led;
                break;
        }
        if(val)
            digitalWrite(act, 1);
        else
            digitalWrite(act, 0);
    }



}
