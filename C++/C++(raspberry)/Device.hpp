#ifndef C_DEVICE_H
#define C_DEVICE_H

#include "utility.hpp"

namespace dev {


    string getMyId() {
        return "idDefault";
    }

    string get_MAC() {
        return util::retrieveMac();
    }

    string get_status() {
        return "on";
    }

}

#endif
