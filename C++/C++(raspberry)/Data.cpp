#include "Data.hpp"
#include <cstdio>
#include <string>
#include <sstream>
#include <forward_list>
#include <iostream>
#include "constants.h"
#include "utility.hpp"
#include "sampler.hpp"


using namespace std;
using namespace util;

namespace data {


    string boolToState(bool state);
    bool stateToBool(string state);


    string Data::get_lastActuatorState() { return boolToState(last_stateActuator); }

    float Data::get_lastMeasure() { return last_measure; }

    string Data::get_measureName() { return measure_name; }

    string Data::get_actuatorName() { return actuator_name; }

    string Data::get_stateActuatorName() { return  state_actuator_name;}
	
	Data::Data(string measure_name, string state_actuator_name, string actuator_name, bool stateActuator): sampler(Sampler::getInstance()), measure_name(measure_name), state_actuator_name(state_actuator_name), actuator_name(actuator_name), stateActuator(stateActuator) {

    }

/*
 * Data Temperature Implementation
 */

    Data_Temp::Data_Temp(): Data("temperature", "stateAirConditioner", "conditioner", false) {}

    float Data_Temp::get_measure() {
        float temp;

        temp = sampler->get_temperature();
        
        last_measure = temp;
        return temp;
    }

    string Data_Temp::get_stateActuator() {

        last_stateActuator = stateActuator;
        return boolToState(stateActuator);
    }

    void Data_Temp::set_stateActuator(string command) {

        stateActuator = stateToBool(command);
        smplr:: set_actuator(stateActuator, TEMP);
    }



/*
 * Data Humidity Implementation
 */

    Data_Hum::Data_Hum(): Data("humidity", "stateHumidifier", "humidifier", false) {}

    float Data_Hum::get_measure() {
        float hum;

        hum = sampler->get_humidity();

        last_measure = hum;
        return hum;
    };

    string Data_Hum::get_stateActuator() {

        last_stateActuator = stateActuator;
        return boolToState(stateActuator);
    }

    void Data_Hum::set_stateActuator(string command) {

        stateActuator = stateToBool(command);
        smplr::set_actuator(stateActuator, HUM);
    }


/*
 * Data Brightness Implementation
 */

    Data_Bright::Data_Bright(): Data("brightness", "stateLight", "light", false) {}

    float Data_Bright::get_measure() {
        float bright;

        bright = sampler->get_luminosity();

        last_measure = bright;
        return bright;
    }

    string Data_Bright::get_stateActuator() {

        last_stateActuator = stateActuator;
        return boolToState(stateActuator);
    }

    void Data_Bright::set_stateActuator(string command) {

        stateActuator = stateToBool(command);
        smplr::set_actuator(stateActuator, LIGHT);

    }


    string toJson(string idDevice, string timestamp_local, forward_list<Data *> &sensors) {

        ostringstream out;
        out << "{\"idDevice\":\"" << idDevice << "\",\"timestamp_local\":\"" << timestamp_local << "\"";
        string json = out.str();
        out.clear();
        forward_list<Data>::iterator sensor;

        for (Data *sensor : sensors) {

            json = json + ",\"" + sensor->get_measureName() + "\":" + std::to_string(sensor->get_measure()) +
                   "," + "\"" + sensor->get_stateActuatorName() + "\":" + "\"" + sensor->get_stateActuator() + "\"";

        }
        return json = json + "}";

    }

    string boolToState(bool state) {
        if (state)
            return "on";
        return "off";
    }

    bool stateToBool(string state){
        if(state.compare("on")==0)
            return true;
        return false;
    }
}


