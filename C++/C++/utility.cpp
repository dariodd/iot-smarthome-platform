#include "utility.hpp"
#include <string>

namespace util {
    std::string get_timestamp() {
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];

        time(&rawtime);
        timeinfo = localtime(&rawtime);

        strftime(buffer, sizeof(buffer), "%d-%m-%Y_%H:%M:%S", timeinfo);
        std::string str(buffer);

        return str;

    }

    std::string generateMac(){
        std::string mac = "";
        char hex[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        srand(time(NULL));

        for(int i=0; i<6; i++) {
            if(i!=0)
                mac = mac + '-';
            for (int j = 0; j < 2; j++)
                mac = mac + hex[rand() % 16];
        }
        return mac;
    }

}
