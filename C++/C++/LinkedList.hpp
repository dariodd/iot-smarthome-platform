#ifndef C_LINKEDLIST_H
#define C_LINKEDLIST_H

#include <string>

namespace linklist {
    struct node {
        std::string data;
        node *next;
    };

    class LinkedList {

    private:
        node *head, *tail, *iterator;
    public:
        LinkedList();

        ~LinkedList();

        void add_node(std::string n);

        void save(std::string name_file);

        void load(std::string name_file);

        void print();

        bool find(std::string id);


    };
}


#endif
