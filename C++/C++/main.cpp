#include <iostream>
#include "kafka.hpp"
#include <cppkafka/cppkafka.h>
#include <thread>
#include "constants.h"
#include "Data.hpp"
#include <vector>
#include <boost/algorithm/string.hpp>
#include "LinkedList.hpp"
#include <forward_list>
#include "Device.hpp"
#include "utility.hpp"

#include <ctime>
#include <cstdlib>
#include "sampler.hpp"


using namespace std;
using namespace linklist;
using namespace data;
using namespace dev;
using namespace util;
using namespace smplr;




string register_Json(string id, string user);



void routine_registration(Configuration& config_receive, Configuration& config_send, string topic_receive, string topic_send, string id);
void routine_sender (Configuration& config, string topic, int production_cycle, string id, forward_list<Data*> sensors);
void routine_receiver_commands(Configuration& config, string topic, forward_list<Data *> actuators);





int main(int argc, char** argv) {
    
    string *id = new string(getMyId());
    int *pc = new int(DEFAULT_PRODUCTION_CYCLE);

    for(int i = 1; i<argc; i++){

        if(argv[i][0]=='-') {
            if (!argv[i][2] == (char) 0) {
                cerr << "Error while passing arguments, usage: " << argv[0] << " [-i] [idDevice]  [-c] [productionCycle]" << endl;
                exit(EXIT_FAILURE);
            }
            switch (argv[i][1]) {
                case 'i':
                    *id = argv[i + 1];
                    break;
                case 'c':
                    if(stoi(argv[i+1])>=30)
                        *pc = stoi(argv[i+1]);
                    else{
                        cout << "Production Cycle value too low, cannot set a production cycle lower than 30 seconds. Production cycle setted to default 30 seconds." << endl;
                    }

            }
        }

    }

    const string myId = *id;
    const string topic_send = KAFKA_TOPIC_SEND;
    const string topic_registration = KAFKA_TOPIC_REGISTRATION;
    const string topic_commands = myId;
    const int production_cycle = *pc;

    delete id;
    delete pc;


    LinkedList registered_devices;
    registered_devices.load("registered_devices");


    forward_list<Data*> sensors;
    Data *dataTemp = new Data_Temp();
    Data *dataHum = new Data_Hum();
    Data *dataBright = new Data_Bright();
    sensors.emplace_front(dataBright);
    sensors.emplace_front(dataHum);
    sensors.emplace_front(dataTemp);



    Configuration configuration_sender = {
            {"bootstrap.servers", KAFKA_BROKER_IP ":" KAFKA_BROKER_PORT}
    };


    Configuration configuration_consumer_registration = {
            {"bootstrap.servers", KAFKA_BROKER_IP ":" KAFKA_BROKER_PORT},
            {"group.id",           myId},
            {"fetch.wait.max.ms",  10},
            {"enable.auto.commit", false},
            {"auto.offset.reset",  "latest"}
    };

    Configuration configuration_consumer_commands = {
            {"bootstrap.servers", KAFKA_BROKER_IP ":" KAFKA_BROKER_PORT},
            {"group.id",           myId},
            {"fetch.wait.max.ms",  10},
            {"enable.auto.commit", false},
            {"auto.offset.reset",  "latest"}
    };



    if(!registered_devices.find(myId)){
        thread thread_registration(routine_registration, ref(configuration_consumer_registration), ref(configuration_sender), topic_registration, topic_send, myId);
        thread_registration.join();
        registered_devices.add_node(myId);
        registered_devices.save("registered_devices");
    }


    thread thread_sender(routine_sender, ref(configuration_sender), topic_send, production_cycle, myId, sensors);
    thread thread_receiver_commands(routine_receiver_commands, ref(configuration_consumer_commands), topic_commands, sensors);

    thread_sender.join();
    thread_receiver_commands.join();


}




string register_Json(string id, string user) {
    ostringstream out;
    out << "{\"idDevice\":\"" << id << "\",\"MAC\":\"" << get_MAC() << "\",\"status\":\"" << get_status() << "\",\"user\":\"" << user << "\"}";
    return out.str();
}


void routine_registration(Configuration& config_receive, Configuration& config_send, string topic_receive, string topic_send, string id) {

    KafkaConsumer consumer(config_receive);

    consumer.subscribe_topic(topic_receive);
    
    double secondsPassed;

    bool exit = false;
    while (!exit) {
        string res = consumer.poll();


        if (!res.compare("nope") == 0) {
            vector <string> registration_info;
            boost::split(registration_info, res, boost::is_any_of("|"));
            if (registration_info[0].compare(id) == 0) {
				clock_t startTime = clock();
				bool flag = true;

				while (flag){
					secondsPassed = (clock() - startTime) / CLOCKS_PER_SEC;
					cout << "press enter to confirm" << endl;
                    if (cin.get()) {
                        cout<<"CONFIRMED"<<endl;
                        KafkaProducer ks(config_send);
                        ks.send("register|" + register_Json(id, registration_info[1]), topic_send);
                        exit = true;
                        flag = false;
                    }

                    if(secondsPassed >= 60){
						cout<<secondsPassed<<"seconds have passed, timeout, retry registration"<<endl;
						flag = false;
					}
				}

            }
        }
    }

};



void routine_sender (Configuration& config, string topic, int production_cycle, string id, forward_list<Data*> sensors) {

    KafkaProducer ks(config);
    ks.send("time|{\"idDevice\":\""+id+"\",\"time\":"+to_string(production_cycle)+"}", topic);

    while (1) {

        ks.send("record|" + toJson(id, get_timestamp(), sensors), topic);
        std::this_thread::sleep_for(std::chrono::seconds(production_cycle));
    }
};

void routine_receiver_commands(Configuration& config, string topic, forward_list<Data *> actuators) {
    KafkaConsumer consumer(config);

    consumer.subscribe_topic(topic);

    while (1) {

        string res = consumer.poll();

        if (!res.compare("nope") == 0) {
            vector<string> command;
            boost::split(command, res, boost::is_any_of("|"));

            for(Data *actuator : actuators){
                if (command[0].compare(actuator->get_actuatorName()) == 0)
                    actuator->set_stateActuator(command[1]);
            }
        }
    }
};


