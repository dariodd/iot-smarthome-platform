#ifndef C_KAFKA_H
#define C_KAFKA_H


#include <string>
#include <cppkafka/cppkafka.h>

using namespace std;
using namespace cppkafka;


class KafkaProducer
{
public:
    KafkaProducer(Configuration conf);
    int send(string message, string topic_name);
private:
    BufferedProducer<string> producer;
};


class KafkaConsumer
{
public:
    KafkaConsumer(Configuration conf);
    bool subscribe_topic(string topic_name);
    bool unsubscribe();
    string poll();
private:
    Consumer consumer;
    bool running = true;
};


#endif