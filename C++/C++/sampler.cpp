#include "sampler.hpp"
#include <iostream>
#include <cstdlib>
#include <cstdint>
#include <thread>
#include "constants.h"
#include "utility.hpp"

using namespace std;

#define MAX_TIMINGS 85



namespace smplr{

    Sampler* Sampler::instance = NULL;

    Sampler::Sampler(){
        this->run(30);
    }


    Sampler * Sampler::getInstance() {
        if (instance == NULL){
            instance = new Sampler();
        }

        return instance;
    }

    void Sampler::read_dht_data(){
        float tem[5] = {20.5, 20.6, 20.7 ,20.6 ,20.5};
        float hum[5] = {50.5, 52.5, 51.9 ,49.5 ,50.1};
        float lum[5] = {20.1, 20.7, 21.5 ,20.4 ,20.1};

        int i=0;
        float c,h,l;

        while(1){

            c=tem[i];
            h=hum[i];
            l=lum[i];
            i++;
            if(i==5)
                i=0;


            cout<<"Humidity = "<<h<<" % Temperature = "<<c<<" *C"<<endl;
            cout<<"Luminosity = " << l << " %"<<endl;

            temperature = c;
            humidity = h;
            luminosity = l;

            this_thread::sleep_for(std::chrono::seconds(time/2));
        }
    }

    void Sampler::run(int sampling_time){
        time = sampling_time;
        thread t1(&Sampler::read_dht_data, this);
        t1.detach();
    }

    float Sampler::get_humidity(){
        return humidity;
    }

    float Sampler::get_luminosity(){
        return luminosity;
    }

    float Sampler::get_temperature(){
        return temperature;
    }

    void set_actuator(bool val, util::action control){
        std::string act;
        std::string state;
        switch(control){
            case util::LIGHT:
                act = "light";
                break;
            case util::HUM:
                act = "humidifier";
                break;
            case util::TEMP:
                act = "conditioner";
                break;
        }
        if(val)
            state="on";
        else
            state="off";

        cout << act << " " << state <<endl;

    }

}
