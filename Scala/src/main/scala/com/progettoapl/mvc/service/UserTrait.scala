package com.progettoapl.mvc.service

import com.progettoapl.mvc.model.User

trait UserTrait {
  def addUser(user: User): User
  def updateUser(id: Int, user: User): User
  def deleteUser(id: Int): Unit
  def UserById(id: Int): User
  def users(): java.lang.Iterable[User]
  def UserByEmail(email: String): java.lang.Iterable[User]
  def UserByUsername(username: String): java.lang.Iterable[User]
}
