package com.progettoapl.mvc.service

import java.lang

import com.progettoapl.mvc.model.Device
import com.progettoapl.mvc.repository.DeviceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class DeviceService(@Autowired deviceRepository: DeviceRepository, @Autowired kafkaTemplate:KafkaTemplate[String,String]) extends DeviceTrait {

  override def addDevice(device: Device): Device = {
    kafkaTemplate.send("register",device.idDevice+"|"+device.user)
    device
  }

  override def updateDevice(id: Int, device: Device): Device = {
    val dev = deviceRepository.findById(id).get()
    if(dev != null){
      dev.MAC = device.MAC
      dev.status = device.status
      dev.user = device.user
    }
    dev
  }

  override def deleteDevice(id: Int): Unit = {
    deviceRepository.deleteById(id)
  }

  override def deviceById(id: Int): Device = {
    deviceRepository.findById(id).get()
  }

  override def devices(): lang.Iterable[Device] = {
    deviceRepository.findAll()
  }

  override def deviceByUsername(username: String): lang.Iterable[Device] = {
    deviceRepository.getAllByUser(username)
  }

  override def deviceByIdDevice(idDevice: String): Device = {
    deviceRepository.getDeviceByIdDevice(idDevice)
  }

  override def deviceByUsernameAndIdDevice(username: String, idDevice: String): Device =  {
    deviceRepository.getDeviceByIdDeviceAndUser(idDevice, username)
  }
}
