package com.progettoapl.mvc.service

import java.lang
import java.util.Collections

import com.progettoapl.mvc.model.User
import com.progettoapl.mvc.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class UserService(@Autowired userRepository: UserRepository) extends UserTrait {
  override def addUser(user: User): User = {
    user.roles = Collections.singletonList("USER")
    userRepository.save(user)
  }

  override def updateUser(id: Int, user: User): User = {
    var us: User = userRepository.findById(id).get()
    if (us != null){
      us.email = user.email
      us.username = user.username
      us.password = user.password
    }
    us
  }

  override def deleteUser(id: Int): Unit = {
    userRepository.deleteById(id)
  }

  override def UserById(id: Int): User = {
    userRepository.findById(id).get()
  }

  override def users(): lang.Iterable[User] = {
    userRepository.findAll()
  }

  override def UserByEmail(email: String): java.lang.Iterable[User] = {
    userRepository.getAllByEmail(email)
  }

  override def UserByUsername(username: String): java.lang.Iterable[User] = {
    userRepository.getAllByUsername(username)
  }
}
