package com.progettoapl.mvc.service

import java.awt.image.BufferedImage
import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import java.lang
import java.text.SimpleDateFormat
import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import scala.collection.JavaConverters._
import com.itextpdf.text.pdf.{PdfPCell, PdfPTable, PdfWriter}
import com.itextpdf.text.{Document, Element, FontFactory, Image, Phrase}
import com.progettoapl.mvc.model.Data
import com.progettoapl.mvc.repository.DataRepository
import org.jfree.chart.{ChartFactory, JFreeChart}
import org.jfree.chart.axis.{DateAxis, NumberAxis}
import org.jfree.chart.plot.{PlotOrientation, XYPlot}
import org.jfree.data.time.{Second, TimeSeries, TimeSeriesCollection}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class DataService(@Autowired dataRepository: DataRepository) extends DataTrait {
  override def findByDevice(idDevice: String): lang.Iterable[Data] = {
    dataRepository.findLastDataByIdDevice(idDevice)
  }

  override def findDataByIdDevice(idDevice: String, time: LocalDateTime): lang.Iterable[Data] = {
    dataRepository.findDataByIdDeviceAndTimestampserver(idDevice, time)
  }

  override def addData(data: Data): Data = {
    dataRepository.save(data)
  }

  override def plotData(data: Iterable[Data]): ByteArrayInputStream = {
    val document = new Document()
    val out = new ByteArrayOutputStream()
    val writer = PdfWriter.getInstance(document, out)
    writer.setStrictImageSequence(true)
    document.open()

    val table = new PdfPTable(4)
    table.setWidthPercentage(80)
    table.setWidths(Array[Int](3, 3, 3, 6))


    val headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD)


    var hcell:PdfPCell = new PdfPCell(new Phrase("Avg. Temperature", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    hcell = new PdfPCell(new Phrase("Avg. Humidity", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    hcell = new PdfPCell(new Phrase("Avg. Brightness", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    hcell = new PdfPCell(new Phrase("Day", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    val dataset_temp: TimeSeriesCollection = new TimeSeriesCollection()
    val temp = new TimeSeries("temperature")

    val dataset_hum: TimeSeriesCollection = new TimeSeriesCollection()
    val hum = new TimeSeries("humidity")

    val dataset_lux: TimeSeriesCollection = new TimeSeriesCollection()
    val lux = new TimeSeries("brightness")

    val lastRecord = Array(0,0,0)
    var avgTemp, avgHum, avgLux, count = 0.0

    for (record <- data) {

      val timestamp = record.timestamp_local

      val zdt = timestamp.atZone(ZoneId.systemDefault)
      val output = Date.from(zdt.toInstant)
      val time:Second = new Second(output)
      temp.addOrUpdate(time, record.temperature)
      hum.addOrUpdate(time, record.humidity)
      lux.addOrUpdate(time, record.brightness)

      if(timestamp.getYear == lastRecord(0) && timestamp.getMonthValue == lastRecord(1) && timestamp.getDayOfMonth == lastRecord(2) ){
        count += 1
        avgTemp += record.temperature; avgHum += record.humidity; avgLux += record.brightness
      }else{
        if(lastRecord(0) != 0){
          var cell: PdfPCell = new PdfPCell(new Phrase("%.2f".format(avgTemp/count)))
          cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER)
          table.addCell(cell)
          cell = new PdfPCell(new Phrase("%.2f".format(avgHum/count)))
          cell.setPaddingLeft(5)
          cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER)
          table.addCell(cell)
          cell = new PdfPCell(new Phrase("%.2f".format(avgLux/count)))
          cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER)
          cell.setPaddingRight(5)
          table.addCell(cell)
          cell = new PdfPCell(new Phrase(lastRecord(0)+"-"+lastRecord(1)+"-"+lastRecord(2)))
          cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
          cell.setHorizontalAlignment(Element.ALIGN_CENTER)
          cell.setPaddingRight(5)
          table.addCell(cell)
        }
        avgTemp = 0.0; avgHum = 0.0; avgLux = 0.0; count = 1.0
        avgTemp = record.temperature; avgHum = record.humidity; avgLux = record.brightness
        lastRecord(0) = timestamp.getYear
        lastRecord(1) = timestamp.getMonthValue
        lastRecord(2) = timestamp.getDayOfMonth
      }

    }

    var cell: PdfPCell = new PdfPCell(new Phrase("%.2f".format(avgTemp/count)))
    cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
    cell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(cell)
    cell = new PdfPCell(new Phrase("%.2f".format(avgHum/count)))
    cell.setPaddingLeft(5)
    cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
    cell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(cell)
    cell = new PdfPCell(new Phrase("%.2f".format(avgLux/count)))
    cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
    cell.setHorizontalAlignment(Element.ALIGN_CENTER)
    cell.setPaddingRight(5)
    table.addCell(cell)
    cell = new PdfPCell(new Phrase(lastRecord(0)+"-"+lastRecord(1)+"-"+lastRecord(2)))
    cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
    cell.setHorizontalAlignment(Element.ALIGN_CENTER)
    cell.setPaddingRight(5)
    table.addCell(cell)

    dataset_temp.addSeries(temp)
    dataset_hum.addSeries(hum)
    dataset_lux.addSeries(lux)
    val tempChart:Image = plotBuild("Temperature Chart","Timestamp", "Temperature", dataset_temp, writer)
    document.add(tempChart)
    val humChart:Image = plotBuild("Humidity Chart","Timestamp", "Humidity %", dataset_hum, writer)
    document.add(humChart)
    val luxChart:Image = plotBuild("Brightness Chart", "Timestamp","Brightness %", dataset_lux, writer)
    document.add(luxChart)

    document.add(table)
    document.close()
    new ByteArrayInputStream(out.toByteArray())
  }


  override def getAllDataToPdf(id: String, time: LocalDateTime): ByteArrayInputStream = {
    val data = findDataByIdDevice(id, time).asScala
    val document = new Document()
    val out = new ByteArrayOutputStream()
    val writer = PdfWriter.getInstance(document, out)
    writer.setStrictImageSequence(true)
    document.open()

    val table = new PdfPTable(4)
    table.setWidthPercentage(80)
    table.setWidths(Array[Int](3, 3, 3, 6))

    val headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD)


    var hcell:PdfPCell = new PdfPCell(new Phrase("Temperature", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    hcell = new PdfPCell(new Phrase("Humidity", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    hcell = new PdfPCell(new Phrase("Brightness", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)

    hcell = new PdfPCell(new Phrase("Timestamp", headFont))
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER)
    table.addCell(hcell)


    for (record <- data) {
      var cell: PdfPCell = new PdfPCell(new Phrase(String.valueOf(record.temperature)))
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
      cell.setHorizontalAlignment(Element.ALIGN_CENTER)
      table.addCell(cell)
      cell = new PdfPCell(new Phrase(String.valueOf(record.humidity)))
      cell.setPaddingLeft(5)
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
      cell.setHorizontalAlignment(Element.ALIGN_CENTER)
      table.addCell(cell)
      cell = new PdfPCell(new Phrase(String.valueOf(record.brightness)))
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
      cell.setHorizontalAlignment(Element.ALIGN_CENTER)
      cell.setPaddingRight(5)
      table.addCell(cell)
      cell = new PdfPCell(new Phrase(String.valueOf(record.timestamp_local)))
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
      cell.setHorizontalAlignment(Element.ALIGN_CENTER)
      cell.setPaddingRight(5)
      table.addCell(cell)
    }

    document.add(table)
    document.close()
    new ByteArrayInputStream(out.toByteArray())

  }

  private def plotBuild(title:String, xLabel:String, yLabel:String, dataset:TimeSeriesCollection, writer: PdfWriter):Image={
    val width: Int = 540
    val height: Int = 350
    val chart: JFreeChart = ChartFactory.createXYLineChart(title, xLabel, yLabel, dataset, PlotOrientation.VERTICAL, true, true, false)
    val plot = chart.getPlot.asInstanceOf[XYPlot]
    val dateAxis = new DateAxis
    dateAxis.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy-HH:mm"))
    plot.setDomainAxis(dateAxis)

    val rangeAxis = plot.getRangeAxis.asInstanceOf[NumberAxis]
    rangeAxis.setRange(0.0,100.0)

    val bufferedImage: BufferedImage = chart.createBufferedImage(width, height)

    val image = Image.getInstance(writer, bufferedImage, 1.0f)
    image.setWidthPercentage(90)
    image.setAlignment(Element.ALIGN_CENTER)

    image
  }


}
