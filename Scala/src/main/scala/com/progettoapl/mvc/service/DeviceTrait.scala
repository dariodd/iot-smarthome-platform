package com.progettoapl.mvc.service

import com.progettoapl.mvc.model.Device

trait DeviceTrait {
  def addDevice(device: Device): Device
  def updateDevice(id: Int, user: Device): Device
  def deleteDevice(id: Int): Unit
  def deviceById(id: Int): Device
  def devices(): java.lang.Iterable[Device]
  def deviceByUsername(username: String): java.lang.Iterable[Device]
  def deviceByIdDevice(idDevice: String): Device
  def deviceByUsernameAndIdDevice(username: String, idDevice: String): Device
}
