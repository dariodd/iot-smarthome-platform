package com.progettoapl.mvc.service

import java.io.ByteArrayInputStream
import java.time.LocalDateTime

import com.progettoapl.mvc.model.Data

trait DataTrait {
  def findByDevice(idDevice: String): java.lang.Iterable[Data]
  def addData(data: Data): Data
  def findDataByIdDevice(idDevice: String, time: LocalDateTime): java.lang.Iterable[Data]
  def plotData(data: Iterable[Data]):ByteArrayInputStream
  def getAllDataToPdf(id:String, time:LocalDateTime):ByteArrayInputStream
}
