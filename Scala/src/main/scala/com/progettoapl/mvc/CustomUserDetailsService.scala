package com.progettoapl.mvc

import java.util

import com.progettoapl.mvc.model.User
import com.progettoapl.mvc.repository.UserRepository
import javax.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.{UserDetailsService, UsernameNotFoundException}
import org.springframework.security.core.{GrantedAuthority, userdetails}
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

@Service
@Transactional
class CustomUserDetailsService(@Autowired val repository: UserRepository) extends UserDetailsService {

  @Override
  def loadUserByUsername (username: String): userdetails.User = {
    val user: User = repository.findByUsername(username)

    if (user == null)
      throw new UsernameNotFoundException("User not found!");

    new org.springframework.security.core.userdetails.User(
      user.username,
      getEncoder().encode(user.password),
      true,
      true,
      true,
      true,
      getAuth(user.roles)
    )
  }

  @Bean
  def getEncoder(): BCryptPasswordEncoder = {
    new BCryptPasswordEncoder();
  }

  def getAuth(rol: util.List[String]): util.List[GrantedAuthority] = {

    var authorities: ArrayBuffer[GrantedAuthority] = new ArrayBuffer[GrantedAuthority]
    var roles: mutable.Buffer[String] = rol.asScala
    for (role <- roles)
      authorities += new SimpleGrantedAuthority(role)

    authorities.asJava

  }

}