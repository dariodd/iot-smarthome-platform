package com.progettoapl.mvc

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}
import org.springframework.security.config.http.SessionCreationPolicy

@Configuration
@EnableWebSecurity
class WebSecurityConfig (@Autowired var customUserDetailsService: CustomUserDetailsService) extends WebSecurityConfigurerAdapter{

  override def configure(auth: AuthenticationManagerBuilder): Unit = {
    auth.userDetailsService(customUserDetailsService)
  }

  override def configure(http: HttpSecurity): Unit = {
    http
      .httpBasic()
      .and()
      .sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
      .authorizeRequests()
      .antMatchers("/user/register").permitAll()
      .antMatchers(HttpMethod.GET, "/user/all").hasAnyAuthority("ADMIN")
      .antMatchers(HttpMethod.GET, "/user/username/{username}").authenticated()
      .antMatchers(HttpMethod.DELETE, "/user/id/{id}").hasAuthority("ADMIN")
      .antMatchers(HttpMethod.POST, "/device/register").hasAnyAuthority("USER","ADMIN")
      .antMatchers(HttpMethod.GET, "/device/all").hasAnyAuthority("ADMIN")
      .antMatchers(HttpMethod.GET, "/device/iddevice/{iddevice}").authenticated()
      .antMatchers(HttpMethod.GET, "/device/username/{username}").authenticated()
      .antMatchers(HttpMethod.DELETE, "/device/id/{id}").hasAnyAuthority("ADMIN")
      .antMatchers(HttpMethod.GET, "/data/id/{id}/time/{time}").authenticated()
      .antMatchers(HttpMethod.GET, "/data/id/{id}/time/{time}/pdfreport").authenticated()
      .antMatchers(HttpMethod.GET, "/test/ping").hasAnyAuthority("ADMIN")
      .antMatchers(HttpMethod.GET, "/test/publish").hasAnyAuthority("ADMIN")
      .antMatchers(HttpMethod.GET, "/test/pdfreport").hasAnyAuthority("ADMIN")
      .anyRequest().authenticated()
      .and()
      .csrf().disable()
  }

}
