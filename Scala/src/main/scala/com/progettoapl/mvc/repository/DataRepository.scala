package com.progettoapl.mvc.repository

import java.time.LocalDateTime

import com.progettoapl.mvc.model.Data
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
trait DataRepository extends CrudRepository[Data, Int]{
  def findAllByIdDevice(idDevice: String): java.lang.Iterable[Data]
  @Query(value = "SELECT * from data d where d.idDevice =:idDevice AND d.timestamp_server >=:time ", nativeQuery = true)
  def findDataByIdDeviceAndTimestampserver(@Param("idDevice") idDevice: String, @Param("time") time: LocalDateTime): java.lang.Iterable[Data]
  @Query(value= "SELECT * from data d where d.idDevice =:idDevice order by id desc limit 1", nativeQuery = true)
  def findLastDataByIdDevice(@Param("idDevice") idDevice: String): java.lang.Iterable[Data]
}
