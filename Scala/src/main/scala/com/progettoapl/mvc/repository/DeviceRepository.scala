package com.progettoapl.mvc.repository

import com.progettoapl.mvc.model.Device
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
trait DeviceRepository extends CrudRepository[Device,Int]{
  def getAllByUser(username: String): java.lang.Iterable[Device]
  def getDeviceByIdDevice(idDevice: String): Device
  def getDeviceByIdDeviceAndUser(idDevice: String,  user: String): Device
}
