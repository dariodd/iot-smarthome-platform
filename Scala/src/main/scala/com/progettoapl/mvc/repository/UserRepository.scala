package com.progettoapl.mvc.repository

import com.progettoapl.mvc.model.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
trait UserRepository extends CrudRepository[User,Int]{
  def getAllByEmail(email: String): java.lang.Iterable[User]
  def getAllByUsername(username: String): java.lang.Iterable[User]
  def findByUsername(username: String): User
}
