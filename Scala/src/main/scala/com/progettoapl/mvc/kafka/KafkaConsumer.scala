package com.progettoapl.mvc.kafka

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.progettoapl.mvc.model.{Data, Device}
import com.progettoapl.mvc.repository.DeviceRepository
import com.progettoapl.mvc.service.DataService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

import org.json4s._
import org.json4s.jackson.JsonMethods._



@Service
class KafkaConsumer(@Autowired dataService: DataService, @Autowired deviceRepository: DeviceRepository ) {
  private implicit val json_formats = org.json4s.DefaultFormats

  @KafkaListener(topics = Array("record_submission"), groupId = "server_id")
  @throws[IOException]
  def consume(message: String): Unit = {
    val messageParts = message.split("\\|")
    messageParts(0) match{
      case "record" => add_data(messageParts(1))
      case "register" => register_device(messageParts(1))
      case "time" => set_time(messageParts(1))
    }
  }

  private def register_device(message: String): Unit ={
    val device: Device = new Device
    val record = parse(message).extract[Map[String, String]]
    device.idDevice = record("idDevice")
    device.MAC = record("MAC")
    device.status = record("status")
    device.user = record("user")
    deviceRepository.save(device)
  }

  private def add_data(json_record: String): Unit ={
    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH:mm:ss")

    val record = parse(json_record).extract[Map[String, Any]]
    val data:Data= new Data
    data.idDevice = record("idDevice").asInstanceOf[String]
    data.brightness = record("brightness").asInstanceOf[Double]
    data.humidity = record("humidity").asInstanceOf[Double]
    data.temperature = record("temperature").asInstanceOf[Double]
    data.stateAirConditioner = record("stateAirConditioner").asInstanceOf[String]
    data.stateHumidifier = record("stateHumidifier").asInstanceOf[String]
    data.stateLight = record("stateLight").asInstanceOf[String]
    data.timestamp_local = LocalDateTime.parse(record("timestamp_local").asInstanceOf[String],formatter)
    data.timestampserver = LocalDateTime.now()
    dataService.addData(data)
  }

  private def set_time(time: String): Unit = {
    val record = parse(time).extract[Map[String, Any]]
    val idDevice = record("idDevice").asInstanceOf[String]
    val samplingTime = record("time").asInstanceOf[BigInt].toInt
    val device = deviceRepository.getDeviceByIdDevice(idDevice)
    if(device != null) {
      device.setTime(samplingTime)
      deviceRepository.save(device)
    }

  }

}
