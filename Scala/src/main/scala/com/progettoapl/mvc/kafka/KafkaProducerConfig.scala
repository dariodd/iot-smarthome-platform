package com.progettoapl.mvc.kafka
import java.util

import org.apache.kafka.clients.producer.ProducerConfig
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.kafka.core.{DefaultKafkaProducerFactory, KafkaTemplate, ProducerFactory}

@Configuration
class KafkaProducerConfig {

  @Bean
  def producerFactory: ProducerFactory[String, String] = {
    val configProps = new util.HashMap[String, AnyRef]
    configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "3.209.53.237:9092")
    configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    new DefaultKafkaProducerFactory[String, String](configProps)
  }

  @Bean
  def kafkaTemplate():KafkaTemplate[String,String] = {
    new KafkaTemplate[String, String](producerFactory)
  }

}
