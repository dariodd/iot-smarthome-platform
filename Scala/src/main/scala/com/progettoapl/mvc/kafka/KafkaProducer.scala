package com.progettoapl.mvc.kafka
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate

@Service
class KafkaProducer(@Autowired  kafkaTemplate:KafkaTemplate[String,String]){

  private val TOPIC = "users"

  def sendMessage(message: String): Unit = {
    kafkaTemplate.send(TOPIC, message)
  }

}