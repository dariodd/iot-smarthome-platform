package com.progettoapl.mvc.controller

import com.progettoapl.mvc.model.User
import com.progettoapl.mvc.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, MediaType, ResponseEntity}
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation._
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority

@Controller
@RequestMapping(path=Array("/user"))
class UserController (@Autowired userService: UserService) {

  @PostMapping(path = Array("/register"), consumes = Array(MediaType.APPLICATION_JSON_VALUE), produces = Array(MediaType.APPLICATION_JSON_VALUE))
  @ResponseBody
  def register(@RequestBody user:User): ResponseEntity[User] = {
    if(user.check && userService.addUser(user) == user)
      new ResponseEntity[User](user, HttpStatus.ACCEPTED)
    else
    new ResponseEntity[User](user, HttpStatus.BAD_REQUEST)
  }

  @GetMapping(path = Array("/all"))
  @ResponseBody
  def getAll(auth: Authentication): java.lang.Iterable[User] = {
    userService.users
  }

  @GetMapping(path = Array("/username/{username}"))
  @ResponseBody
  def getByUsername(@PathVariable username: String, auth: Authentication): ResponseEntity[java.lang.Iterable[User]] = {
    if(username == auth.getName || auth.getAuthorities.contains(new SimpleGrantedAuthority("ADMIN"))) {
      val users = userService.UserByUsername(username)
      if(users != null)
        return new ResponseEntity[java.lang.Iterable[User]](users, HttpStatus.ACCEPTED)
    }
    new ResponseEntity[java.lang.Iterable[User]](HttpStatus.NOT_FOUND)
  }

  @DeleteMapping(path = Array("id/{id}"))
  @ResponseBody
  def deleteUser(@PathVariable id: Int): String = {
    userService.deleteUser(id)
    "User with id: "+id.toString+" deleted"
  }



}
