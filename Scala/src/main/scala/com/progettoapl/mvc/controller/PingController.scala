package com.progettoapl.mvc.controller

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import com.itextpdf.text.{BaseColor, Chunk, Document, Font, FontFactory}
import com.itextpdf.text.pdf.PdfWriter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.{HttpHeaders, MediaType, ResponseEntity}
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, ResponseBody}

@Controller
@RequestMapping(path=Array("/test"))
class PingController(@Autowired  kafkaTemplate:KafkaTemplate[String,String] ) {

  private val TOPIC = "users"

  @GetMapping(path = Array("/ping"))
  @ResponseBody
  def ping() = "pong"


  @GetMapping(value = Array("/publish"))
  @ResponseBody
  def sendMessageToKafkaTopic(): Unit = {
    val message = "hello world!"
    println(message)
    kafkaTemplate.send(TOPIC,message)
  }


  @GetMapping(value = Array("/pdfreport"), produces = Array(MediaType.APPLICATION_PDF_VALUE))
  def testReportPdf():ResponseEntity[InputStreamResource] = {
    val document: Document= new Document();
    val out: ByteArrayOutputStream = new ByteArrayOutputStream();
    PdfWriter.getInstance(document, out);
    document.open()
    val font: Font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
    val chunk:Chunk = new Chunk("Hello World", font);
    document.add(chunk);
    document.close();
    val bis = new ByteArrayInputStream(out.toByteArray());
    val headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=pdfreport.pdf");
    ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
  }


}
