package com.progettoapl.mvc.controller

import java.lang

import com.progettoapl.mvc.model.Device
import com.progettoapl.mvc.service.DeviceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpStatus, MediaType, ResponseEntity}
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation._

@Controller
@RequestMapping(path=Array("/device"))
class DeviceController (@Autowired deviceService: DeviceService) {


  @PostMapping(path = Array("/register"), consumes = Array(MediaType.APPLICATION_JSON_VALUE), produces = Array(MediaType.APPLICATION_JSON_VALUE))
  @ResponseBody
  def register(@RequestBody device: Device, auth: Authentication): ResponseEntity[Device] = {
    device.setUser(auth.getName)
    println(device.idDevice)
    if(deviceService.deviceByIdDevice(device.idDevice) == null) {
      return new ResponseEntity[Device](deviceService.addDevice(device), HttpStatus.ACCEPTED)
    }
    new ResponseEntity[Device](HttpStatus.BAD_REQUEST)
  }

  @GetMapping(path = Array("/all"))
  @ResponseBody
  def getAll(): java.lang.Iterable[Device] = {
    deviceService.devices
  }

  @GetMapping(path = Array("/iddevice/{iddevice}"))
  @ResponseBody
  def getBy(@PathVariable iddevice: String, auth: Authentication): ResponseEntity[Device] = {
    if(auth.getAuthorities.contains(new SimpleGrantedAuthority("ADMIN")))
      new ResponseEntity[Device](deviceService.deviceByIdDevice(iddevice), HttpStatus.ACCEPTED)
    else
      new ResponseEntity[Device](deviceService.deviceByUsernameAndIdDevice(auth.getName, iddevice), HttpStatus.ACCEPTED)
  }

  @GetMapping(path = Array("/username/{username}"))
  @ResponseBody
  def getByUsername(@PathVariable username: String, auth: Authentication): ResponseEntity[java.lang.Iterable[Device]] = {
    if(auth.getName == username)
      new ResponseEntity[java.lang.Iterable[Device]]( deviceService.deviceByUsername(username),HttpStatus.ACCEPTED)
    else
      new ResponseEntity[lang.Iterable[Device]](HttpStatus.NOT_FOUND)
  }

  @DeleteMapping(path = Array("/id/{id}"))
  @ResponseBody
  def deleteDevice(@PathVariable id: Int): String = {
    deviceService.deleteDevice(id)
    "Device with id: "+id.toString+" deleted"
  }

}