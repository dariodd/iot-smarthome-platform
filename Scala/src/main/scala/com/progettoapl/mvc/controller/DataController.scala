package com.progettoapl.mvc.controller

import java.lang
import java.time.{LocalDateTime}

import com.progettoapl.mvc.model.{Data, Device}
import com.progettoapl.mvc.service.{DataService, DeviceService}
import scala.collection.JavaConverters._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{GetMapping, PathVariable, PostMapping, RequestBody, RequestMapping, ResponseBody}
import org.springframework.security.core.Authentication
import org.springframework.http.{HttpHeaders, HttpStatus, MediaType, ResponseEntity}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.springframework.core.io.InputStreamResource


@Controller
@RequestMapping(path=Array("/data"))
class DataController (@Autowired dataService: DataService, @Autowired deviceService: DeviceService, @Autowired kafkaTemplate: KafkaTemplate[String,String] ) {

  @GetMapping(path = Array("/id/{id}/time/{time}"))
  @ResponseBody
  def getData(auth: Authentication, @PathVariable id: String, @PathVariable time: Int): ResponseEntity[java.lang.Iterable[Data]] = {
    val device: Device = deviceService.deviceByUsernameAndIdDevice(auth.getName,id)
    if(device != null ){
      if(time <= 0){
        val data = dataService.findByDevice(id)
        if(!data.asScala.isEmpty)
          return new ResponseEntity[java.lang.Iterable[Data]](data, HttpStatus.ACCEPTED)
      }else {
        val date = LocalDateTime.now.minusDays(time)
        val data = dataService.findDataByIdDevice(id, date)
        if(!data.asScala.isEmpty)
          return new ResponseEntity[java.lang.Iterable[Data]](data, HttpStatus.ACCEPTED)
      }
    }
    new ResponseEntity[lang.Iterable[Data]](HttpStatus.BAD_REQUEST)
  }

  @GetMapping(value = Array("/id/{id}/time/{time}/pdfreport"), produces = Array(MediaType.APPLICATION_PDF_VALUE))
  def pdfReport(auth: Authentication, @PathVariable id: String, @PathVariable time: Int):ResponseEntity[InputStreamResource] = {
    val device: Device = deviceService.deviceByUsernameAndIdDevice(auth.getName,id)
    if(device == null || time <= 0){
      new ResponseEntity[InputStreamResource](HttpStatus.BAD_REQUEST)
    }else {
      val data = dataService.findDataByIdDevice(id, LocalDateTime.now.minusDays(time)).asScala
      if(data.isEmpty)
        return new ResponseEntity[InputStreamResource](HttpStatus.BAD_REQUEST)
      val bis = dataService.plotData(data)
      val headers = new HttpHeaders()
      headers.add("Content-Disposition", "inline; filename=pdfreport.pdf")
      ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis))
    }
  }


  @GetMapping(path=Array("/id/{id}"))
  @ResponseBody
  def getLastData(auth: Authentication, @PathVariable id:String):ResponseEntity[Data] = {
    val device: Device = deviceService.deviceByUsernameAndIdDevice(auth.getName,id)
    if(device != null){
      return new ResponseEntity[Data](dataService.findByDevice(id).iterator.next,HttpStatus.ACCEPTED);
    }
    new ResponseEntity[Data](HttpStatus.NOT_FOUND)
  }


  @PostMapping(path= Array("/id/{id}/commands"),consumes = Array(MediaType.APPLICATION_JSON_VALUE))
  @ResponseBody
  def stateTest(auth: Authentication, @PathVariable id:String, @RequestBody commands: String): ResponseEntity[String] ={
    val device: Device = deviceService.deviceByUsernameAndIdDevice(auth.getName,id)
    if(device != null) {
      implicit val formats = org.json4s.DefaultFormats
      val test = parse(commands).extract[Map[String, String]]
      for ((k, v) <- test) {
        kafkaTemplate.send(id, k + "|" + v)
      }
      return new ResponseEntity[String]("Request done", HttpStatus.ACCEPTED)
    }
    new ResponseEntity[String]("Device not found!", HttpStatus.NOT_FOUND)
  }

}