package com.progettoapl.mvc


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import springfox.documentation.swagger2.annotations.EnableSwagger2




@Configuration
@EnableJpaRepositories(Array("com.progettoapl.mvc.repository"))
@SpringBootApplication
@EnableSwagger2
class ScalaProjectApp {

}

object ScalaProjectApp {
  def main(args: Array[String]): Unit = {
    val configClasses: Array[Class[_]] = Array(classOf[ScalaProjectApp])
    SpringApplication.run(configClasses, args);
  }
}