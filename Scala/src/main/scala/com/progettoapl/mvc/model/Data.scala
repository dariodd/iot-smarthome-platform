package com.progettoapl.mvc.model

import java.time.LocalDateTime

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.{Column, Entity, GeneratedValue, GenerationType, Id, Table}

import scala.beans.BeanProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "data")
@Entity
class Data() extends Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @BeanProperty
  @Column(name = "ID")
  var id: Int = _

  @Column(name = "IDDEVICE")
  @BeanProperty
  var idDevice: String = _

  @Column(name = "TIMESTAMP_SERVER")
  @BeanProperty
  var timestampserver: LocalDateTime = _

  @Column(name = "TIMESTAMP_LOCAL")
  @BeanProperty
  var timestamp_local: LocalDateTime = _

  @Column(name = "TEMPERATURE")
  @BeanProperty
  var temperature: Double = _

  @Column(name = "STATEAIRCONDITIONER")
  @BeanProperty
  var stateAirConditioner: String = _

  @Column(name = "HUMIDITY")
  @BeanProperty
  var humidity: Double = _

  @Column(name = "STATEHUMIDIFIER")
  @BeanProperty
  var stateHumidifier: String = _

  @Column(name = "BRIGHTNESS")
  @BeanProperty
  var brightness: Double = _

  @Column(name = "STATELIGHT")
  @BeanProperty
  var stateLight: String = _

}