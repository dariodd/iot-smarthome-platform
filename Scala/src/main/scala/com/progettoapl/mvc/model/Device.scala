package com.progettoapl.mvc.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence._

import scala.beans.BeanProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "devices")
@Entity
class Device() extends Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @BeanProperty
  @Column(name = "ID")
  var id: Int = _

  @Column(name="IDDEVICE", unique = true)
  @BeanProperty
  var idDevice: String = _

  @Column(name = "MAC")
  @BeanProperty
  var MAC: String = _

  @Column(name = "USER")
  @BeanProperty
  var user: String = _

  @Column(name = "STATUS")
  @BeanProperty
  var status: String = _

  @Column(name = "TIME")
  @BeanProperty
  var time: Int = _

}
