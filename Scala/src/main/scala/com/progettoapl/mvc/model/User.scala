package com.progettoapl.mvc.model

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}
import javax.persistence._

import scala.beans.BeanProperty


@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "users")
@Entity
class User() extends Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @BeanProperty
  @Column(name = "ID")
  var id: Int = _

  @Column(name = "USERNAME", unique = true)
  @BeanProperty
  var username: String = _

  @Column(name = "EMAIL", unique = true)
  @BeanProperty
  var email: String = _

  @Column(name = "PASSWORD")
  @BeanProperty
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  var password: String = _

  @ElementCollection
  var roles: java.util.List[String] =  _


  def check(): Boolean ={
    if(this.username == "" || this.password == "" || this.password.length < 8 || !check_password()){
      false
    }
    else true
  }

  private def check_password(): Boolean ={
    """(\w+)@([\w\.]+)""".r.unapplySeq(this.email).isDefined
  }

}
